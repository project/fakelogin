<?php

namespace Drupal\fakelogin\Routing;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 * Alter default user login route.
 */
class RouteSubscriber extends RouteSubscriberBase {

  /**
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {

    // Replace the default user login form with a fake form.
    if ($route = $collection->get('user.login')) {
      $route->setDefault('_form', '\Drupal\fakelogin\Form\FakeLoginForm');
    }

    // Disable access to the registration page.
    if ($route = $collection->get('user.register')) {
      $route->setRequirement('_access', 'FALSE');
    }

    // Disable access to the password page.
    if ($route = $collection->get('user.pass')) {
      $route->setRequirement('_access', 'FALSE');
    }
  }

}
