<?php

namespace Drupal\fakelogin\Routing;

use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;

/**
 * Defines dynamic routes.
 */
class FakeLoginRoutes {

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $route_collection = new RouteCollection();
    $config = \Drupal::config('fakelogin.settings');

    $real_login_path = $config->get('login_path') ?? "/real-login-path";
    $real_login_route = new Route(
      $real_login_path,
      [
        '_form' => '\Drupal\user\Form\UserLoginForm',
        '_title' => 'Log in',
      ],
      [
        '_user_is_logged_in' => 'FALSE',
      ],
      [
        '_maintenance_access' => TRUE,
      ]
    );
    $route_collection->add('fakelogin.reallogin', $real_login_route);

    return $route_collection;
  }

}
