<?php

namespace Drupal\fakelogin\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteBuilderInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure settings for fake login.
 *
 * @internal
 */
class FakeLoginConfigForm extends ConfigFormBase {

  /**
   * The route building service.
   *
   * @var \Drupal\Core\Routing\RouteBuilderInterface
   */
  protected $routeBuilder;

  /**
   * Constructs a new Drupal\fakelogin\Form\FakeLoginConfigForm object.
   *
   * @param \Drupal\Core\Routing\RouteBuilderInterface $routeBuilder
   *   The router builder service.
   */
  public function __construct(RouteBuilderInterface $routeBuilder) {
    $this->routeBuilder = $routeBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('router.builder')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'fakelogin_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['fakelogin.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form = parent::buildForm($form, $form_state);

    $config = $this->config('fakelogin.settings');

    $form['login_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Set real login Path'),
      '#default_value' => $config->get('login_path') ?? "/real-login-path",
      '#required' => TRUE,
      '#description' => $this->t('Please provide a valid path start with "/".'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('fakelogin.settings');
    $config->set('login_path', $form_state->getValue('login_path'));
    $config->save();
    $this->routeBuilder->rebuild();

    return parent::submitForm($form, $form_state);
  }

}
